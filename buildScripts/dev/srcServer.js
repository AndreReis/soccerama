import chalk from 'chalk';
import express from 'express';
import path from 'path';
import open from 'open';
import webpack from 'webpack';
import config from '../../webpack.config.dev';
import devMiddleware from 'webpack-dev-middleware';
import hotMiddleware from 'webpack-hot-middleware';

/* eslint-disable no-console */

const port = 3000;
const app = express();
const compiler = webpack(config);

app.use(devMiddleware(compiler, {
  hot: true,
  filename: config.output.filename,
  publicPath: config.output.publicPath,
  noInfo: config.noInfo,
  stats: {
    colors: true
  },
  historyApiFallback: true
}));

app.use(hotMiddleware(compiler, {
  log: console.log,
  path: '/__webpack_hmr',
  heartbeat: 10 *1000
}));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../../src/index.html'));
});

app.get('/:file(*.mst)', (req, res) => {
  let file = req.params.file;
  res.sendFile(path.join(__dirname, `../../src/views/${file}`));
});

app.listen(port, function (err) {
  if (err) {
    console.log(chalk.magenta(`error: ${err}`));
  } else {
    console.log(chalk.cyan(`Listening on port ${port}`));
    open('http://localhost:' + port)
  }
});
