import chalk from 'chalk';
import express from 'express';
import path from 'path';
import open from 'open';
import compression from 'compression';

/* eslint-disable no-console */

const port = 3069;
const app = express();

app.use(compression());
app.use(express.static('dist'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../../src/index.html'));
});

app.get('/:file(*.mst)', (req, res) => {
  let file = req.params.file;
  res.sendFile(path.join(__dirname, `../../src/views/${file}`));
});

app.listen(port, function (err) {
  if (err) {
    console.log(chalk.magenta(`error: ${err}`));
  } else {
    console.log(chalk.cyan(`Listening on port ${port}`));
    open('http://localhost:' + port)
  }
});
