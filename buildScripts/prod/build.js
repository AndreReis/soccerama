/* eslint-disable no-console */
import chalk from 'chalk';
import webpack from 'webpack';
import config from '../../webpack.config.prod';

process.env.NODE_ENV = 'production';

console.log(chalk.blue('Generating minified bundle for production, this might take a while...'));

webpack(config).run(
  (err, stats) => {
    if (err) {
      console.log(chalk.red(err));
      return 1;
    }

    const jsonStats = stats.toJson();

    if (jsonStats.hasErrors) {
      return jsonStats.errors.map(error => console.log(chalk.red(error)));
    }

    if (jsonStats.hasWarnings) {
      console.log(chalk.yellow('Webpack generated with the following warnings'));
      jsonStats.warnings.map(warning => console.log(chalk.red(warning)));
    }

    console.log(chalk.gray(`Webpack stats ${stats}`));

    // if we got here means build has succeeded
    console.log(chalk.green('App packaged and ready to go, written to folder dist in your project!'));

    return 0;
  });

