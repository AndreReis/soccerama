export let standings = [
  {
    position: 1,
    team_id: 939,
    team_name: 'Midtjylland',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 12,
      won: 8,
      draw: 1,
      lost: 3,
      goals_scored: 28,
      goals_against: 17
    },
    home: {
      games_played: 6,
      won: 5,
      draw: 1,
      lost: 0,
      goals_scored: 18,
      goals_against: 7
    },
    away: {
      games_played: 6,
      won: 3,
      draw: 0,
      lost: 3,
      goals_scored: 10,
      goals_against: 10
    },
    total: {
      goal_difference: '+11',
      points: 25
    },
    result: 'Promotion - Superliga (Championship Group)',
    points: 25,
    recent_form: 'WWWWL',
    status: 'up',
    team: {
      data: {
        id: 939,
        legacy_id: 513,
        name: 'Midtjylland',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1999,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/11/939.png',
        venue_id: 342
      }
    }
  },
  {
    position: 2,
    team_id: 2394,
    team_name: 'Nordsjælland',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 7,
      draw: 2,
      lost: 2,
      goals_scored: 28,
      goals_against: 18
    },
    home: {
      games_played: 6,
      won: 4,
      draw: 1,
      lost: 1,
      goals_scored: 13,
      goals_against: 8
    },
    away: {
      games_played: 5,
      won: 3,
      draw: 1,
      lost: 1,
      goals_scored: 15,
      goals_against: 10
    },
    total: {
      goal_difference: '+10',
      points: 23
    },
    result: 'Promotion - Superliga (Championship Group)',
    points: 23,
    recent_form: 'WDLDW',
    status: 'same',
    team: {
      data: {
        id: 2394,
        legacy_id: null,
        name: 'Nordsjælland',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 2003,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/26/2394.png',
        venue_id: 1436
      }
    }
  },
  {
    position: 3,
    team_id: 293,
    team_name: 'Brøndby',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 6,
      draw: 3,
      lost: 2,
      goals_scored: 21,
      goals_against: 10
    },
    home: {
      games_played: 6,
      won: 5,
      draw: 1,
      lost: 0,
      goals_scored: 16,
      goals_against: 3
    },
    away: {
      games_played: 5,
      won: 1,
      draw: 2,
      lost: 2,
      goals_scored: 5,
      goals_against: 7
    },
    total: {
      goal_difference: '+11',
      points: 21
    },
    result: 'Promotion - Superliga (Championship Group)',
    points: 21,
    recent_form: 'WWDDW',
    status: 'up',
    team: {
      data: {
        id: 293,
        legacy_id: null,
        name: 'Brøndby',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1964,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/5/293.png',
        venue_id: 5659
      }
    }
  },
  {
    position: 4,
    team_id: 211,
    team_name: 'Horsens',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 5,
      draw: 4,
      lost: 2,
      goals_scored: 15,
      goals_against: 10
    },
    home: {
      games_played: 5,
      won: 3,
      draw: 2,
      lost: 0,
      goals_scored: 9,
      goals_against: 3
    },
    away: {
      games_played: 6,
      won: 2,
      draw: 2,
      lost: 2,
      goals_scored: 6,
      goals_against: 7
    },
    total: {
      goal_difference: '+5',
      points: 19
    },
    result: 'Promotion - Superliga (Championship Group)',
    points: 19,
    recent_form: 'WWDDL',
    status: 'same',
    team: {
      data: {
        id: 211,
        legacy_id: 631,
        name: 'Horsens',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1994,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/19/211.png',
        venue_id: 5661
      }
    }
  },
  {
    position: 5,
    team_id: 85,
    team_name: 'København',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 5,
      draw: 3,
      lost: 3,
      goals_scored: 21,
      goals_against: 14
    },
    home: {
      games_played: 6,
      won: 3,
      draw: 3,
      lost: 0,
      goals_scored: 13,
      goals_against: 7
    },
    away: {
      games_played: 5,
      won: 2,
      draw: 0,
      lost: 3,
      goals_scored: 8,
      goals_against: 7
    },
    total: {
      goal_difference: '+7',
      points: 18
    },
    result: 'Promotion - Superliga (Championship Group)',
    points: 18,
    recent_form: 'LWWWL',
    status: 'down',
    team: {
      data: {
        id: 85,
        legacy_id: 146,
        name: 'København',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1992,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/21/85.png',
        venue_id: 5660
      }
    }
  },
  {
    position: 6,
    team_id: 1703,
    team_name: 'Hobro',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 5,
      draw: 3,
      lost: 3,
      goals_scored: 16,
      goals_against: 14
    },
    home: {
      games_played: 6,
      won: 4,
      draw: 1,
      lost: 1,
      goals_scored: 12,
      goals_against: 6
    },
    away: {
      games_played: 5,
      won: 1,
      draw: 2,
      lost: 2,
      goals_scored: 4,
      goals_against: 8
    },
    total: {
      goal_difference: '+2',
      points: 18
    },
    result: 'Promotion - Superliga (Championship Group)',
    points: 18,
    recent_form: 'DLLWW',
    status: 'same',
    team: {
      data: {
        id: 1703,
        legacy_id: 517,
        name: 'Hobro',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1913,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/7/1703.png',
        venue_id: 849
      }
    }
  },
  {
    position: 7,
    team_id: 1789,
    team_name: 'OB',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 3,
      draw: 5,
      lost: 3,
      goals_scored: 11,
      goals_against: 10
    },
    home: {
      games_played: 6,
      won: 2,
      draw: 2,
      lost: 2,
      goals_scored: 6,
      goals_against: 6
    },
    away: {
      games_played: 5,
      won: 1,
      draw: 3,
      lost: 1,
      goals_scored: 5,
      goals_against: 4
    },
    total: {
      goal_difference: '+1',
      points: 14
    },
    result: 'Superliga (Relegation Group)',
    points: 14,
    recent_form: 'DWLDD',
    status: 'up',
    team: {
      data: {
        id: 1789,
        legacy_id: 520,
        name: 'OB',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1889,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/29/1789.png',
        venue_id: 914
      }
    }
  },
  {
    position: 8,
    team_id: 2905,
    team_name: 'AGF',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 4,
      draw: 2,
      lost: 5,
      goals_scored: 14,
      goals_against: 16
    },
    home: {
      games_played: 6,
      won: 2,
      draw: 1,
      lost: 3,
      goals_scored: 6,
      goals_against: 9
    },
    away: {
      games_played: 5,
      won: 2,
      draw: 1,
      lost: 2,
      goals_scored: 8,
      goals_against: 7
    },
    total: {
      goal_difference: '-2',
      points: 14
    },
    result: 'Superliga (Relegation Group)',
    points: 14,
    recent_form: 'WLWLD',
    status: 'up',
    team: {
      data: {
        id: 2905,
        legacy_id: 629,
        name: 'AGF',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1902,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/25/2905.png',
        venue_id: 1708
      }
    }
  },
  {
    position: 9,
    team_id: 1020,
    team_name: 'AaB',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 2,
      draw: 6,
      lost: 3,
      goals_scored: 11,
      goals_against: 15
    },
    home: {
      games_played: 5,
      won: 2,
      draw: 2,
      lost: 1,
      goals_scored: 7,
      goals_against: 7
    },
    away: {
      games_played: 6,
      won: 0,
      draw: 4,
      lost: 2,
      goals_scored: 4,
      goals_against: 8
    },
    total: {
      goal_difference: '-4',
      points: 12
    },
    result: 'Superliga (Relegation Group)',
    points: 12,
    recent_form: 'LWDWD',
    status: 'same',
    team: {
      data: {
        id: 1020,
        legacy_id: 173,
        name: 'AaB',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1885,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/28/1020.png',
        venue_id: 85765
      }
    }
  },
  {
    position: 10,
    team_id: 2650,
    team_name: 'Lyngby',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 3,
      draw: 3,
      lost: 5,
      goals_scored: 16,
      goals_against: 24
    },
    home: {
      games_played: 5,
      won: 3,
      draw: 1,
      lost: 1,
      goals_scored: 10,
      goals_against: 8
    },
    away: {
      games_played: 6,
      won: 0,
      draw: 2,
      lost: 4,
      goals_scored: 6,
      goals_against: 16
    },
    total: {
      goal_difference: '-8',
      points: 12
    },
    result: 'Superliga (Relegation Group)',
    points: 12,
    recent_form: 'WLDWD',
    status: 'up',
    team: {
      data: {
        id: 2650,
        legacy_id: 627,
        name: 'Lyngby',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1921,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/26/2650.png',
        venue_id: 1576
      }
    }
  },
  {
    position: 11,
    team_id: 390,
    team_name: 'SønderjyskE',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 12,
      won: 2,
      draw: 5,
      lost: 5,
      goals_scored: 19,
      goals_against: 21
    },
    home: {
      games_played: 6,
      won: 1,
      draw: 3,
      lost: 2,
      goals_scored: 8,
      goals_against: 7
    },
    away: {
      games_played: 6,
      won: 1,
      draw: 2,
      lost: 3,
      goals_scored: 11,
      goals_against: 14
    },
    total: {
      goal_difference: '-2',
      points: 11
    },
    result: 'Superliga (Relegation Group)',
    points: 11,
    recent_form: 'LLDDL',
    status: 'same',
    team: {
      data: {
        id: 390,
        legacy_id: null,
        name: 'SønderjyskE',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 2004,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/6/390.png',
        venue_id: 5653
      }
    }
  },
  {
    position: 12,
    team_id: 86,
    team_name: 'Silkeborg',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 3,
      draw: 1,
      lost: 7,
      goals_scored: 12,
      goals_against: 20
    },
    home: {
      games_played: 5,
      won: 3,
      draw: 0,
      lost: 2,
      goals_scored: 9,
      goals_against: 8
    },
    away: {
      games_played: 6,
      won: 0,
      draw: 1,
      lost: 5,
      goals_scored: 3,
      goals_against: 12
    },
    total: {
      goal_difference: '-8',
      points: 10
    },
    result: 'Superliga (Relegation Group)',
    points: 10,
    recent_form: 'LLWLL',
    status: 'same',
    team: {
      data: {
        id: 86,
        legacy_id: 522,
        name: 'Silkeborg',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 1917,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/22/86.png',
        venue_id: 85371
      }
    }
  },
  {
    position: 13,
    team_id: 8635,
    team_name: 'FC Helsingør',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 3,
      draw: 0,
      lost: 8,
      goals_scored: 10,
      goals_against: 25
    },
    home: {
      games_played: 5,
      won: 2,
      draw: 0,
      lost: 3,
      goals_scored: 5,
      goals_against: 12
    },
    away: {
      games_played: 6,
      won: 1,
      draw: 0,
      lost: 5,
      goals_scored: 5,
      goals_against: 13
    },
    total: {
      goal_difference: '-15',
      points: 9
    },
    result: 'Superliga (Relegation Group)',
    points: 9,
    recent_form: 'LLLLW',
    status: 'same',
    team: {
      data: {
        id: 8635,
        legacy_id: null,
        name: 'FC Helsingør',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 2005,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/27/8635.png',
        venue_id: 5602
      }
    }
  },
  {
    position: 14,
    team_id: 2356,
    team_name: 'Randers',
    group_id: null,
    group_name: null,
    overall: {
      games_played: 11,
      won: 1,
      draw: 4,
      lost: 6,
      goals_scored: 7,
      goals_against: 15
    },
    home: {
      games_played: 5,
      won: 0,
      draw: 3,
      lost: 2,
      goals_scored: 1,
      goals_against: 5
    },
    away: {
      games_played: 6,
      won: 1,
      draw: 1,
      lost: 4,
      goals_scored: 6,
      goals_against: 10
    },
    total: {
      goal_difference: '-8',
      points: 7
    },
    result: 'Superliga (Relegation Group)',
    points: 7,
    recent_form: 'LLDWL',
    status: 'same',
    team: {
      data: {
        id: 2356,
        legacy_id: 514,
        name: 'Randers',
        twitter: null,
        country_id: 320,
        national_team: false,
        founded: 2003,
        logo_path: 'https://cdn.sportmonks.com/images/soccer/teams/20/2356.png',
        venue_id: 1414
      }
    }
  }
];