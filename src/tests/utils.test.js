import {expect as Expect} from 'chai';
import * as UtilFuncs from '../scripts/utilFuncs.js';
import * as Data from './data.js';

describe('sort data', () => {
  it('should be sorted asc by won games - position', () => {
    let dataStandings = Data.standings.slice(0),
        result = true;

    UtilFuncs.sortViewData('overall__won', dataStandings, 'asc', false, 'position');

    for (let i = 1; i < dataStandings.length; i++) {
      let first = dataStandings[i - 1],
          second = dataStandings[i];

      if (first.overall.won > second.overall.won
          || first.overall.won === second.overall.won
          && first.position > second.position) {
        result = false;
        break;
      }
    }

    Expect(result)
      .to
      .equal(true);
  });
});
