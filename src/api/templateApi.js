import 'whatwg-fetch';
import chalk from 'chalk';
import getBaseURL from './baseUrl';

const baseURL = getBaseURL('template');

let onSucess = response => {
  return response.text()
}

let onError = error => {
  console.log(chalk.bgMagenta(error)); //eslint-disable-line no-console
}

export let get = url => {
  return fetch(baseURL + url).then(onSucess, onError);
}