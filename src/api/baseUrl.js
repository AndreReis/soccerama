export default function getBaseURL(type) {
  let apiUrl = 'https://soccer.sportmonks.com/api/v2.0/';

  return type === 'template'
    ? '/'
    : apiUrl;
}