import 'whatwg-fetch';
import $ from 'jquery';
import NProgress from 'nprogress';
import Promise from 'es6-promise';
import getBaseURL from './baseUrl';

const BASE_URL = getBaseURL(),
      API_TOKEN = 'HOLCAStI6Z0OfdoPbjdSg5b41Q17w2W5P4WuoIBdC66Z54kUEvGWPIe33UYC';

let loading = false,
    $nprogress;

let startProgress = () => {
  NProgress.start();
  $nprogress = $('#nprogress').find('.bar');
  loading = true;
  setTimeout(incrementProgress, 100);
}

let incrementProgress = () => {
  if (loading) {
    NProgress.inc();
    setTimeout(incrementProgress, 100);
  }
}

let endProgress = success => {
  $nprogress.addClass(success && 'success' || 'error');
  setTimeout(() => {
    loading = false;
    NProgress.done();
  }, 100);
}

let onSuccess = response => {
  return new Promise((resolve, reject) => {
    if (response.ok) {
      endProgress(true);
      response.json()
        .then(result => {
          resolve(result.data);
        })
        .catch(() => {
          resolve(undefined);
        });
    } else {
      endProgress(false);
      reject(response);
    }
  })

}

let onError = error => {
  endProgress(false);
  return new Promise((resolve, reject) => {
    reject(error);
  });
}

let addToken = url => {
  // if url already has request params. add token with &
  return /\?/.test(url)
    && `${url}&api_token=${API_TOKEN}`
    || `${url}?api_token=${API_TOKEN}`;
}

let get = url => {
  startProgress();
  let apiUrl = addToken(`${BASE_URL}${url}`)
  return fetch(apiUrl).then(onSuccess,onError);
}

export let getSeasons = () => {
  return get('seasons?include=league');
}

export let getLeagueStandings = season => {
  return get(`standings/season/${season}?include=standings.team`);
}

export let getLeagueTopScorers = season => {
  return get(`topscorers/season/${season}?include=goalscorers.player,goalscorers.team,assistscorers.player,assistscorers.team`);
}

export let getTeamInfo = teamId => {
  return get(`teams/${teamId}?include=squad,country,squad.player,squad.player.position`);
}

export let getPlayerInfo = playerId => {
  return get(`players/${playerId}?include=team,position`);
}