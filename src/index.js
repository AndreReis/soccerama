import './index.less';
import * as SeasonManager from './scripts/seasonManager.js';
import * as StandingManager from './scripts/standingManager.js';
import * as TeamInfoProvider from './scripts/teamInfoProvider.js';
import * as PlayerInfoProvider from './scripts/playerInfoProvider.js';
import * as TopScorerManager from './scripts/topScorerManager.js';

SeasonManager.initialize();
StandingManager.initialize();
TeamInfoProvider.initialize();
PlayerInfoProvider.initialize();
TopScorerManager.initialize();