import $ from 'jquery';
import '../../node_modules/jquery-ui-dist/jquery-ui.min.js';
import * as Utils from './utils.js';
import * as WebApi from '../api/webApi.js';
import * as TemplateApi from '../api/templateApi.js';
import * as StandingManager from './standingManager.js';
import * as TopScorerManager from './topScorerManager.js';

let $container = $('#season-manager-container'),
    seasonData,
    seasonManagerTemplate;

// prepares object to group combobox by league
let prepareSeasonData = seasons => {
  let result = [],
      currentLeague = {},
      currentLeagueName;

  seasons.forEach(season => {
    let leagueName = season.league.data.name;

    if (leagueName === currentLeagueName) {
      currentLeague.seasons.push(season);
    }
    else {
      currentLeagueName = leagueName;

      if (Object.keys(currentLeague).length > 0) {
        result.push(currentLeague);
      }

      currentLeague = {
        name: currentLeagueName,
        seasons: [season]
      }
    }
  });

  result.push(currentLeague);

  return result;
}

let setView = seasons => {
  seasonData = seasons;
  let leagues = prepareSeasonData(seasons),
      firstSelectedSeason = seasons[seasons.length - 1];

  Utils.applyTemplate($container, seasonManagerTemplate, { leagues });
  StandingManager.loadSeason(firstSelectedSeason.id);
  TopScorerManager.loadSeason(firstSelectedSeason.id);

  $container.children('#season-select').selectmenu({
    select: (e, ui) => {
      let seasonId = ui.item.element.data('seasonid');
      StandingManager.loadSeason(seasonId);
      TopScorerManager.loadSeason(seasonId);

      let season = Utils.getObjectsByFieldValue(seasonData, 'id', [seasonId])[0];
      setSelectableText(season);
    }
  });

  setSelectableText(firstSelectedSeason);
}

let setSelectableText = season => {

  $('#season-select-button')
    .children('.ui-selectmenu-text')
    .html(`${season.league.data.name} - ${season.name}`);
}

export let initialize = () => {
  TemplateApi.get('seasonManager.mst')
    .then(template => {
      seasonManagerTemplate = template;
      WebApi.getSeasons()
        .then(setView)
        .catch(error => console.log(error));
    });
}