import $ from 'jquery';
import * as Utils from './utils.js';
import * as WebApi from '../api/webApi.js';
import * as TemplateApi from '../api/templateApi.js';
import * as PlayerInfoProvider from './playerInfoProvider.js';

let $container = $('#topscorers-manager-container'),
    firstLoad = true,
    $topGoalScorersTable,
    $topAssistScorersTable,
    topScorersTableTemplate,
    topScorerBodyTemplate,
    seasonData;

let setView = () => {
  let goalData = seasonData.goalscorers.data,
      assistData = seasonData.assistscorers.data,
      hasGoalData = goalData.length > 0,
      hasAssistData = assistData.length > 0;

  // only apply the table template on the first load
  if (firstLoad || (!hasGoalData && !hasAssistData)) {
    firstLoad = !hasGoalData && !hasAssistData;
    Utils.applyTemplate($container, topScorersTableTemplate, { hasGoalData, hasAssistData });
    $topGoalScorersTable = $container.find('#topgoalscorers-table');
    $topAssistScorersTable = $container.find('#topassistscorers-table');
    Utils.setSortable($topGoalScorersTable, 'topgoalscorers', setView, 'position', () => seasonData.goalscorers.data);
    Utils.setSortable($topAssistScorersTable, 'topassistscorers', setView, 'position', () => seasonData.assistscorers.data);
  }

  if (hasGoalData) {
    Utils.applyTemplate($topGoalScorersTable
      .children('.body'), topScorerBodyTemplate, { topScorers: goalData });
    Utils.showElement($topGoalScorersTable.parent());
    setPlayerNameModal($topGoalScorersTable);
  }
  else {
    Utils.hideElement($topGoalScorersTable.parent());
  }

  if (hasAssistData) {
    Utils.applyTemplate($topAssistScorersTable
      .children('.body'), topScorerBodyTemplate, { topScorers: assistData });
    Utils.showElement($topAssistScorersTable.parent());
    setPlayerNameModal($topAssistScorersTable);
  }
  else {
    Utils.hideElement($topAssistScorersTable.parent());
  }
}

let setPlayerNameModal = ($table) => {
  $table.off('click.playerName').on('click.playerName', '.player-name', e => {
    let $target = $(e.target),
        $teamRow = $target.parent(),
        playerId = $teamRow.data('id');

    if ($teamRow.is('.row')) {
      PlayerInfoProvider.showPlayer(playerId);
    }
  });
};

export let initialize = () => {
  TemplateApi.get('topScorerTable.mst')
    .then(template => {
      topScorersTableTemplate = template;
        return TemplateApi.get('topScorerBody.mst');
    })
    .then(template => {
      topScorerBodyTemplate = template;
    });
};

export let loadSeason = (seasonId) => {
  WebApi.getLeagueTopScorers(seasonId)
    .then(season => {
      seasonData = season.length > 0
                && Utils.getObjectsByFieldValue(season, 'stage_name', ['1st Phase', 'Regular Season'])[0].standings.data
                || season;
      setView();
    })
    .catch(error => console.log(error));
}