import * as Utils from './utils.js';
import * as WebApi from '../api/webApi.js';
import * as TemplateApi from '../api/templateApi.js';

let playerInfoTemplate;

export let showPlayer = playerId => {
  WebApi.getPlayerInfo(playerId)
    .then(player => {
      Utils.openModal(playerInfoTemplate, player);
    })
    .catch(error => console.log(error));
};

export let initialize = () => {
  TemplateApi.get('playerInfo.mst')
    .then(template => {
      playerInfoTemplate = template;
    });
};