import $ from 'jquery';
import * as Utils from './utils.js';
import * as WebApi from '../api/webApi.js';
import * as TemplateApi from '../api/templateApi.js';
import * as TeamInfoProvider from './teamInfoProvider.js';

let $container = $('#standing-manager-container'),
    reloadView = true,
    $standingsTable,
    standingsTableTemplate,
    standingsBodyTemplate,
    seasonData;

let setView = () => {
  let seasonHasData = seasonData.length > 0;
  // only apply the table template on the first load
  if (reloadView || !seasonHasData) {
    reloadView = !seasonHasData;
    Utils.applyTemplate($container, standingsTableTemplate, {seasonHasData});
    $standingsTable = $container.children('#standings-table');
    Utils.setSortable($standingsTable, 'standings', setView, 'position', () => seasonData);
  }

  if (seasonHasData) {
    let data = { standings: seasonData };

    Utils.applyTemplate($standingsTable.children('.body'), standingsBodyTemplate, data);
    setTeamNameModal();
  }
}

let setTeamNameModal = () => {
  $standingsTable.off('click.teamName').on('click.teamName', '.team-name', e => {
    let $target = $(e.target),
        $teamRow = $target.parent(),
        teamId = $teamRow.data('id');



    if ($teamRow.is('.row')) {
      TeamInfoProvider.showTeam(teamId);
    }
  });
}

export let initialize = () => {
  TemplateApi.get('standingsTable.mst')
      .then(template => {
        standingsTableTemplate = template;
        return TemplateApi.get('standingsBody.mst');
      })
      .then(template => {
        standingsBodyTemplate = template;
      });
      
}

export let loadSeason = (seasonId) => {
  WebApi.getLeagueStandings(seasonId)
    .then(season => {
      seasonData = season.length > 0
                && Utils.getObjectsByFieldValue(season, 'stage_name', ['1st Phase', 'Regular Season'])[0].standings.data
                || season;

      setView();
    });
    // .catch(error => console.log(error));
}