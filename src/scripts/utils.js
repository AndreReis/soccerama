import $ from 'jquery';
import Mustache from 'mustache';
import * as UtilFuncs from './utilFuncs.js';

let $modalOverlay = $('.modal-overlay'),
    $modalWindow = $('.modal-window'),
    $closeButton = $modalWindow.find('.button-close'),
    $backButton = $modalWindow.find('.button-back'),
    currentModalState = {},
    modalStates = [];

let closeModal = () => {
  modalStates = [];
  currentModalState = {};
  $('body').css('overflow', 'auto');
  hideElement($modalOverlay);
};

$closeButton.click(closeModal);
$backButton.click(() => goToPreviousModal());

$modalOverlay.on('click', e => {
  if (e.target.isSameNode($modalOverlay[0])) {
    closeModal();
  }
});

$(document).keydown(e => {
  switch(e.which) {
    case 27: // escape key pressed
      closeModal();
      break;
    case 8: // backspace key pressed
      if (modalStates.length > 0) {
        goToPreviousModal();
        e.preventDefault();
        e.stopPropagation();
      }
      else if (Object.keys(currentModalState).length > 0) {
        closeModal();
        e.preventDefault();
        e.stopPropagation();
      }
      break;
  }
});

let elementVisibility = ($element, visible) => {
  visible && $element.removeClass('hidden') || $element.addClass('hidden');
};

let goToPreviousModal = () => {
  currentModalState = modalStates.pop();

  currentModalState.setViewFunc();
  
  if (modalStates.length === 0) {
    hideElement($backButton);
  }
}

export let setSortable = ($table, name, setViewFunc, tieBreakerField, sortDataFunc) => {
  $table.off(`click.${name}`).on(`click.${name}`, '.sortable', e => {
    let target = e.target,
        $target = $(target);

    if ($target.hasClass('fa') || $target.is('span')) {
      $target = $target.parent();
      target = $target[0];
    }

    let sortField = $target.data('field'),
        sortDir = reverseDir($target.data('dir'));

    // remove the dir attribute and icon from every column
    $table
      .find('.sortable')
        .each((index, el) => {
          delete el.dataset.dir;
        })
      .children('.fa')
        .removeClass(getDirClass('asc'))
        .removeClass(getDirClass('desc'));

    // add the dir attribute and icon to the current column
    $target
      .data('dir', sortDir);
    $target
      .children('.fa')
        .addClass(getDirClass(sortDir));

    // sort array and reset the table view
    UtilFuncs.sortViewData(sortField, sortDataFunc(), sortDir, target.dataset.hasOwnProperty('parseint'), tieBreakerField);
    setViewFunc();
  });
}

export let applyTemplate = ($element, template, data, partials) => {
  var html = Mustache.render(template, data, partials);
  $element.html(html);
};

export let showElement = $element => {
  elementVisibility($element, true);
};

export let hideElement = $element => {
  elementVisibility($element, false);
};

export let openModal = (template, data, setViewFunc) => {
  if ((!setViewFunc || setViewFunc !== currentModalState.setViewFunc) && Object.keys(currentModalState).length > 0) {
    modalStates.push(currentModalState);
    showElement($backButton);
  }

  currentModalState = { setViewFunc };

  applyTemplate($modalWindow.children('.content'), template, data);

  $('body').css('overflow', 'hidden');

  showElement($modalOverlay);
}

export let getObjectsByFieldValue = (list, field, value) => {
  return list.filter(obj => value.includes(obj[field]));
}

export let reverseDir = dir => {
  // the first dir is always asc
  return dir && {'asc': 'desc', 'desc': 'asc'}[dir] || 'asc';
};

export let getDirClass = dir => ({'asc': 'fa-caret-up', 'desc': 'fa-caret-down'}[dir]);
