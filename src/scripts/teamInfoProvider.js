import $ from 'jquery';
import * as Utils from './utils.js';
import * as WebApi from '../api/webApi.js';
import * as TemplateApi from '../api/templateApi.js';
import * as PlayerInfoProvider from './playerInfoProvider.js';

let firstLoad = true,
    $playersTable,
    teamInfoTableTemplate,
    templateInfoBodyTemplate,
    teamData;

let setView = team => {
  if (firstLoad) {
    firstLoad = false;
    teamData = team || teamData;
    Utils.openModal(teamInfoTableTemplate, teamData, setView);
    $playersTable = $('#players-table');
  }

  Utils.applyTemplate($playersTable.children('.body'), templateInfoBodyTemplate, teamData);
  Utils.setSortable($playersTable, 'teaminfo', setView, 'number', () => teamData.squad.data);
  setPlayerNameModal();
};

let setPlayerNameModal = () => {
  $playersTable.off('click.playerName').on('click.playerName', '.player-name', e => {
    let $target = $(e.target),
        $teamRow = $target.parent(),
        playerId = $teamRow.data('id');

    if ($teamRow.is('.row')) {
      firstLoad = true;
      PlayerInfoProvider.showPlayer(playerId);
    }
  });
};

export let showTeam = teamId => {
  firstLoad = true;
  WebApi.getTeamInfo(teamId)
    .then(setView)
    .catch(error => console.log(error));
};

export let initialize = () => {
  TemplateApi.get('teamInfoTable.mst')
    .then(template => {
      teamInfoTableTemplate = template;
    });
  TemplateApi.get('teamInfoBody.mst')
    .then(template => {
      templateInfoBodyTemplate = template;
    });
};