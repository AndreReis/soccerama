export let sortViewData = (sortField, sortData, sortDir, isParseInt, tieBreakerField) => {
  sortData.sort((first, second) => {
      // if field is deeper than first level, sortField will
      // have __ to separate levels
      let sortFieldArray = sortField.split('__'),
          firstVal = first,
          secondVal = second;

      // cycle through the levels specified in sortField
      for (let i = 0; i < sortFieldArray.length; i++) {
        let field = sortFieldArray[i];
        firstVal = firstVal[field];
        secondVal = secondVal[field];
      }

      // if it's the goal difference column
      // the values have to be parsed for a proper sorting
      if (isParseInt) {
        firstVal = parseInt(firstVal);
        secondVal = parseInt(secondVal);
      }

      // orders first by field, either asc or desc
      return firstVal < secondVal
              && (sortDir === 'asc' && -1 || 1)
          || firstVal > secondVal
              && (sortDir === 'desc' && -1 || 1)
          // if field values are equal, orders by tieBreakerField asc
          || (first[tieBreakerField] < second[tieBreakerField] && -1 || 1);
    });
}