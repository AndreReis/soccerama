import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

export default {
  debug : true,
  devtool : 'inline-source-map',
  noInfo : true,
  entry : [
    'webpack/hot/dev-server',
    'webpack-hot-middleware/client?reload=true&path=/__webpack_hmr&timeout=2000',
     path.resolve(__dirname, 'src/index')
  ],
  target : 'web',
  output : {
    path: path.resolve(__dirname, 'src'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins : [
    //
    new ExtractTextPlugin('[name].css'),

    // Create HTML file that includes reference to output file
    new HtmlWebpackPlugin({template: 'src/index.html', inject: true}),

    // Enable hot reloading while developing
    new webpack
      .optimize
      .OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module : {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel']
      }, {
        test: /\.less$/,
        loader: 'style!css!less'
      },
      {
        // Capture eot, ttf, svg, woff, and woff2
        test: /\.(eot|ttf|svg|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
        options: {
          name: 'fonts/[hash].[ext]'
        }
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'file?hash=/static/images/sha512&digest=hex&name=[hash].[ext]',
          'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
        ]
      }
    ]
  }
}
